<?php
/**
 * Plugin Name: World Map for Podlove
 * Description:       Draw a World Map from Podlove analytics data
 * Version:           0.0.1
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Thomas W-P
 * Author URI:        https://thomas.w-p.me.uk
 * 
 
World Map for Podlove is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
World Map for Podlove is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

include_once 'SettingsPage.php';
$settings_page = new SettingsPage();



function world_map_for_podlove_shortcode($atts = [], $content = null, $tag = '')
{
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);

    $wm4pl_options = get_option( 'wm4pl_option_name',
            array(
                'ocean_colour' => '#f9e6ff',
                'land_colour'  => '#f2ccff',
                'dot_colour' => '#4d0066',
                'country_colour' => '#e699ff',
                'num_downloads' => 1000,
            ));
   
    global $wpdb;
    //the only query
    $sql = "SELECT `accessed_at`,`lng`,`lat` FROM `wp_podlove_downloadintentclean` " .
            "WHERE `lat` IS NOT Null ORDER BY `accessed_at` DESC LIMIT " . 
            $wm4pl_options['num_downloads'] . ";";
    $results = $wpdb->get_results($sql);
    
    $o .= "<style>.ocean { fill: " . $wm4pl_options['ocean_colour'] ."; } .countries { fill: " . $wm4pl_options['land_colour'] . "; }"
            . " .country.active { fill: " . $wm4pl_options['country_colour'] ."; } .point { fill: " . $wm4pl_options['dot_colour'] . "</style>";
    $o .= '  <div id="inputs" class="control-group">' . "\r\n";
    $o .= '    <form method="post" action="options.php">' . "\r\n";
    $o .= '      <textarea name="pasted" id="pasted" class="input-block-level" style="display:none" placeholder="Longitude,Latitude">' . "\r\n";

    foreach ( $results as $result ) 
    {
        $o .= $result->lng . "," . $result->lat . "\r\n";
    }

    $o .= '</textarea>' . "Click a country to zoom; drag to pan. \r\n";
    $o .= '      <input type="button" value="Plot points" onclick="plot_points()"  style="display:none"  id="plotbutton" class="btn">' . "\r\n";
    $o .= '<span class="sliders pull-right">' . "\r\n";
    $o .= '        <span class="slidertext">Adjust point size:</span>' . "\r\n";
    $o .= '<span class="slider">' . "\r\n";
    $o .= '          <input type="range" value="2" min="1" max="4" step=".5" id="point_size_slider">' . "\r\n";
    $o .= '        </span>' . "\r\n";
    $o .= '<span class="slidertext last">Adjust point opacity:</span>' . "\r\n";
    $o .= '<span class="slider">' . "\r\n";
    $o .= '          <input type="range" value=".6" min=".2" max="1" step=".1" id="point_opacity_slider">' . "\r\n";
    $o .= '        </span>' . "\r\n";
    $o .= '      </span>' . "\r\n";
    $o .= '    </form>' . "\r\n";
    $o .= '  </div>' . "\r\n";
    $o .= '</div>' . "\r\n";
    $o .= '<!-- SVG spawned into this div -->' . "\r\n";
    $o .= '<div class="svg_container wide"></div>' . "\r\n";
    $o .= '<div class="thin">' . "\r\n";
    $o .= '  <div class="footer"></div>' . "\r\n";
    $o .= '  <small class="pull-right">Latest ';
    $o .= $wm4pl_options['num_downloads'];
    $o .= ' downloads. Inspired by <a href="http://dwtkns.com/pointplotter/">Point Plotter</a> by <a href="https://twitter.com/dwtkns">@dwtkns</a>.</small>';
    $o .= '  </div>' . "\r\n";
    
    $o .= '<script>' . "\r\n";
    $o .= 'var width = $(\'.svg_container\').width(),
    height = width/2,
    base_point_radius = 2,
    base_point_opacity = .6,
    translation = {
      x: width/2,
      y: height/2,
      reset: function() { translation.x = width/2; translation.y = height/2; }
    },
    zoomed;

var proj      = d3.geo.eckert4()
                  .scale( (width - 50) / 5.33) // eckert4 map width ~= scale * 5.33...
                  .translate([0,0]),
    path      = d3.geo.path()
                  .projection(proj)
                  .pointRadius(base_point_radius),
    graticule = d3.geo.graticule();

var drag      = d3.behavior.drag()
                  .on(\'drag\', dragmove);

var svg         = d3.select(".svg_container").append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    .call(drag),
    bg          = svg.append("rect").attr("class", "background")
                      .attr("width", width)
                      .attr("height", height)
                      .on("click", click),
    translator  = svg.append("g").attr("class","translator")
                      .attr("transform", "translate(" + translation.x + "," + translation.y + ")")
    map         = translator.append("g").attr("class","map"),
    ocean       = map.append("path").attr("class", "ocean noclicks"),
    countries   = map.append("g")   .attr("class","countries"),
    grat        = map.append("path").attr("class", "graticule noclicks"),
    point_group = map.append("g")   .attr("class","points noclicks");

var points = []

queue()' . "\r\n";
$o .= '    .defer(d3.json, "' . substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT'])) . '/data/world-110m.json")' . "\r\n";


$o .= '    .await(ready);

function ready(error, world) {
  ocean.datum(graticule.outline).attr("d", path);
  grat.datum(graticule).attr("d", path);

  countries
    .selectAll("path")
      .data(topojson.object(world, world.objects.countries).geometries)
    .enter().append("path")
      .attr("class", "country")
      .attr("d", path)
      .on("click", click);
}

function plot_points(){ 
  var pasted = $(\'#pasted\').val(),
      rows;

  if( pasted.match(/[^0-9\-\.\,\t\s\r\n]/) ) plotting_error();
  else if (pasted.indexOf(",")  >= 0) {
    $(\'#inputs\').removeClass(\'error\').addClass(\'success\')
    plotPoints(d3.csv.parseRows(pasted));
  }
  else if (pasted.indexOf("\t") >= 0) {
    $(\'#inputs\').removeClass(\'error\').addClass(\'success\')
    plotPoints(d3.tsv.parseRows(pasted));
  }
  else plotting_error();
  
  function plotting_error() {
    $(\'#inputs\').removeClass(\'success\').addClass(\'error\')
    $(\'#pasted\').val("Failed to parse coordinates...")
  }

  function plotPoints(rows) {
    points = [];
    rows.forEach( function(pt) {
      pt[0] = +pt[0]
      pt[1] = +pt[1]

      points.push({
        geometry: { type: \'Point\',
                    coordinates: pt },
        type: \'Feature\'
        // ,properties: { }
      });
    })
  }
  refresh();
} 

// rotate map on drag, or move \'viewport\' if zoomed in
function dragmove() {
  var Δ = { 
          x: d3.event.dx,
          y: d3.event.dy  
        },
        scaling = 0.15;

  if (zoomed) {
      translation.x += Δ.x;
      translation.y += Δ.y;
      translator.attr("transform", "translate(" + translation.x + "," + translation.y + ")")    
  }

  else {
    var start = { 
          lon: proj.rotate()[0], 
          lat: proj.rotate()[1]
        },
        end = { 
          lon: start.lon + Δ.x * scaling, 
          lat: start.lat - Δ.y * scaling 
        };
    // use start.lat instead of end.lat to prevent vertical movement
    proj.rotate([end.lon,start.lat])
  }
  refresh();
}

// from http://bl.ocks.org/mbostock/2206590
function click(d) {
  var x = 0,
      y = 0,
      k = 1;
  if (d && zoomed !== d) {
    var centroid = path.centroid(d);
    x = -centroid[0];
    y = -centroid[1];
    k = 4;
    zoomed = d;
  } else {
    zoomed = null;
  }

  countries.selectAll("path")
      .classed("active", zoomed && function(d) { return d === zoomed; });

  translation.reset();
  translator.transition()
    .duration(700)
    .attr("transform", "translate(" + translation.x + "," + translation.y + ")")

  map.transition()
    .duration(700)
    .attr("transform", "scale(" + k + ")translate(" + x + "," + y + ")")
  
  countries.transition()
    .duration(700)
    .style("stroke-width", .5 / k + "px");

  grat.transition()
    .duration(700)
    .style("stroke-width", 1 / k + "px");

  path.pointRadius(base_point_radius/k)
  point_group.selectAll(".point").transition().duration(700).attr("d",path)
}

// rescale map on resize
// $(window).resize(function() {
//   var new_width = $(\'.svg_container\').width(),
//       new_height = new_width/2;
//   svg.attr("width", new_width)
//      .attr("height", new_height);
//   map.attr("transform","scale(" + new_width/width + ")")
// });

$("#point_size_slider").change(function() {
  var k = zoomed ? 4 : 1;
  base_point_radius = $(this).val();
  path.pointRadius(base_point_radius/k)
  point_group.selectAll(".point").attr("d",path)
})

$("#point_opacity_slider").change(function() {
  base_point_opacity = $(this).val();
  point_group.selectAll(".point").style("opacity",base_point_opacity)
})

function refresh() {
    var pts = point_group.selectAll(".point").data(points);
      pts.exit().remove();
      pts.enter().append("path").attr("class", "point");
      pts.attr("d",path);
    map.selectAll(".ocean").attr("d",path)
    map.selectAll(".land").attr("d",path)
    map.selectAll(".graticule").attr("d",path)
    map.selectAll(".country").attr("d",path)
  }' . "\r\n";
    $o .= 'plot_points()' . "\r\n";
    $o .= '</script>' . "\r\n";
    
    return $o;

}

function world_map_for_podlove_init()
{
    add_shortcode('world_map_for_podlove', 'world_map_for_podlove_shortcode');
}
 
add_action('init', 'world_map_for_podlove_init');





/**
 * Enqueue plugin style-file
 */
function world_map_for_podlove_styles_scripts() {
    // Respects SSL, Style.css is relative to the current file
    wp_register_style( 'world_map_for_podlove_style', plugins_url('/styles/site-style.css', __FILE__) );
    wp_enqueue_style( 'world_map_for_podlove_style' );
    //wp_register_style( 'world_map_for_podlove_bootstrap-style', plugins_url('/css/bootstrap.css', __FILE__) );
    //wp_enqueue_style( 'world_map_for_podlove_bootstrap-style' );
    // scripts
    wp_register_script( 'world_map_for_podlove_js1', plugins_url('/js/bootstrap.min.js', __FILE__) );
    wp_register_script( 'world_map_for_podlove_js2', plugins_url('/js/d3.geo.projection.v0.min.js', __FILE__) );
    wp_register_script( 'world_map_for_podlove_js3', plugins_url('/js/d3.v3.min.js', __FILE__) );
    wp_register_script( 'world_map_for_podlove_js4', plugins_url('/js/jquery-latest.js', __FILE__) );
    wp_register_script( 'world_map_for_podlove_js5', plugins_url('/js/queue.v1.min.js', __FILE__) );
    wp_register_script( 'world_map_for_podlove_js6', plugins_url('/js/topojson.v0.min.js', __FILE__) );

    
    //the order turns out to be important!
    wp_enqueue_script( 'world_map_for_podlove_js4' );
    wp_enqueue_script( 'world_map_for_podlove_js3' );
    wp_enqueue_script( 'world_map_for_podlove_js5' );
    wp_enqueue_script( 'world_map_for_podlove_js6' );
    wp_enqueue_script( 'world_map_for_podlove_js1' );
    wp_enqueue_script( 'world_map_for_podlove_js2' );

}

/**
 * Register with hook 'wp_enqueue_scripts', which can be used for front end CSS and JavaScript
 */
add_action( 'wp_enqueue_scripts', 'world_map_for_podlove_styles_scripts' );

/**
 * Add Settings link to plugins - code from GD Star Ratings
 */
 function add_settings_link($links, $file) {
    static $this_plugin;
    if (!$this_plugin) $this_plugin = plugin_basename(__FILE__);

    if ($file == $this_plugin){
    $settings_link = '<a href="options-general.php?page=tptp-wm4pl">'.__("Settings", "world_map_for_podlove").'</a>';
     array_unshift($links, $settings_link);
    }
    return $links;
 }
 
 add_filter('plugin_action_links', 'add_settings_link', 10, 2 );
 
 
add_action( 'admin_enqueue_scripts', 'mw_enqueue_color_picker' );
function mw_enqueue_color_picker( $hook_suffix ) {
    // first check that $hook_suffix is appropriate for your admin page
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'world_map_for_podlove_js8', plugins_url('js/init-colour-picker.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
}
 
