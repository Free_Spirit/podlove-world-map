**World Map for Podlove**

Simple World Map for Podlove podcasting tool
---

## Installing

You need to clone the project and put all the folders in
the wp-content/plugins folder. 

The plugin can then be activated and then put on any page with the shortcode:
[world_map_for_podlove]

N.B. This does NOT conform to Wordpress Plugin standards since it uses an outdated version of jquery. Feel free to clone in an fix this.

---

