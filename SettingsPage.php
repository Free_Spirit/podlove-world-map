<?php

/**
 * Podlove World Map
 *
 * @package    Podloove World Map
 * @subpackage Podloove World Map Settings
 * 


World Map for Podlove is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
World Map for Podlove is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
*/

/**
 * Thanks to
 * https://codex.wordpress.org/Creating_Options_Pages#Example_.232
 *
 * @author thomas
 */
 



class SettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'World Map for Podlove Options', 
            'World Map for Podlove', 
            'manage_options', 
            'tptp-wm4pl',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'wm4pl_option_name',
            array(
                'ocean_colour' => '#f9e6ff',
                'land_colour'  => '#f2ccff',
                'dot_colour' => '#4d0066',
                'country_colour' => '#e699ff',
                'num_downloads' => 1000,
            ));
        ?>
        <div class="wrap">
	<h1>World Map for Podlove</h1>
        <h2>Usage</h2>
        <p>Use shortcode [world_map_for_podlove]. </p>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'my_option_group' );
                do_settings_sections( 'tptp-wm4pl' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'my_option_group', // Option group
            'wm4pl_option_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Colours', // Title
            array( $this, 'print_section_info' ), // Callback
            'tptp-wm4pl' // Page
        );  

        add_settings_field(
            'ocean_colour', // ID
            'Ocean Colour:', // Title 
            array( $this, 'ocean_colour_callback' ), // Callback
            'tptp-wm4pl', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'land_colour', 
            'Land Colour:', 
            array( $this, 'land_colour_callback' ), 
            'tptp-wm4pl', 
            'setting_section_id'
        );      
        
        add_settings_field(
            'dot_colour', 
            'Dot Colour:', 
            array( $this, 'dot_colour_callback' ), 
            'tptp-wm4pl', 
            'setting_section_id'
        ); 
        
        add_settings_field(
            'country_colour', 
            'Selected Country Colour:', 
            array( $this, 'country_colour_callback' ), 
            'tptp-wm4pl', 
            'setting_section_id'
        ); 
        
        add_settings_section(
            'dots_section_id', // ID
            'Number of Downloads/Dots', // Title
            array( $this, 'print_dots_section_info' ), // Callback
            'tptp-wm4pl' // Page
        );  
        
        add_settings_field(
            'num_downloads', 
            'Number of dots:', 
            array( $this, 'num_downloads_callback' ), 
            'tptp-wm4pl', 
            'dots_section_id'
        ); 
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['ocean_colour'] ) )
            $new_input['ocean_colour'] = sanitize_text_field( $input['ocean_colour'] );

        if( isset( $input['land_colour'] ) )
            $new_input['land_colour'] = sanitize_text_field( $input['land_colour'] );
        
        if( isset( $input['dot_colour'] ) )
            $new_input['dot_colour'] = sanitize_text_field( $input['dot_colour'] );
                
        if( isset( $input['country_colour'] ) )
            $new_input['country_colour'] = sanitize_text_field( $input['country_colour'] );
        
        if( isset( $input['num_downloads'] ) )
            $new_input['num_downloads'] = absint( $input['num_downloads'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print '<p>Choose colours for the map.</p>' . "\r\n";
    }

        /** 
     * Print the Section text
     */
    public function print_dots_section_info()
    {
        print '<p>Choose how many dots to show. This will always be the most recent downloads.</p>' . "\r\n";
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function ocean_colour_callback()
    {
        printf(
            '<input type="text" size=10 maxlength=7 id="ocean_colour" name="wm4pl_option_name[ocean_colour]" value="%s" class="my-color-field"/>',
            isset( $this->options['ocean_colour'] ) ? esc_attr( $this->options['ocean_colour']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function land_colour_callback()
    {
        printf(
            '<input type="text" size=10 maxlength=7  id="land_colour" name="wm4pl_option_name[land_colour]" value="%s" class="my-color-field"/>',
            isset( $this->options['land_colour'] ) ? esc_attr( $this->options['land_colour']) : ''
        );
    }
    
    /** 
     * Get the settings option array and print one of its values
     */
    public function dot_colour_callback()
    {
        printf(
            '<input type="text" size=10 maxlength=7  id="dot_colour" name="wm4pl_option_name[dot_colour]" value="%s" class="my-color-field"/>',
            isset( $this->options['dot_colour'] ) ? esc_attr( $this->options['dot_colour']) : ''
        );
    }
    /** 
     * Get the settings option array and print one of its values
     */
    public function country_colour_callback()
    {
        printf(
            '<input type="text" size=10 maxlength=7  id="country_colour" name="wm4pl_option_name[country_colour]" value="%s" class="my-color-field"/>',
            isset( $this->options['country_colour'] ) ? esc_attr( $this->options['country_colour']) : ''
        );
    }
    /** 
     * Get the settings option array and print one of its values
     */
    public function num_downloads_callback()
    {
        printf(
            '<input type="text" size=10 maxlength=7  id="country_colour" name="wm4pl_option_name[num_downloads]" value="%s"/>',
            isset( $this->options['num_downloads'] ) ? esc_attr( $this->options['num_downloads']) : ''
        );
    }
}

